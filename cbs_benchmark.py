import signal
import time
from contextlib import contextmanager

import cbs
import cbs_constraints_vertex
import cbs_wait
import input_maker
import visualize


class TimeoutException(Exception):
    pass


@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")

    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)


if __name__ == '__main__':
    robot_n = [10, 8, 6, 4]
    maps = ['map_complex']
    cbs_func = [cbs.main, cbs_wait.main, cbs_constraints_vertex.main]
    test_n = 30
    result = []

    for test_map in maps:
        result.append(test_map + '\n')
        for robot_num in robot_n:
            result.append(str(robot_num) + "대" + '\n')
            i = 0
            while i < test_n:
                input_maker.main(test_map, robot_num)
                try:
                    with time_limit(60):
                        cbs_constraints_vertex.main()
                except TimeoutException as e:
                    print("cbs main " + str(e))
                    continue
                result.append(str(i + 1) + "회차" + '\t')
                for func in cbs_func:
                    cost, planning_time = func()
                    if visualize.main():
                        success_rate = "1"
                    else:
                        success_rate = "0"
                    result.append(cost + '\t' + planning_time + '\t' + success_rate + '\t')
                result.append('\n')
                with open("benchmark_"+test_map+".csv", 'a') as f:
                    f.writelines(result)
                result.clear()
                i += 1
